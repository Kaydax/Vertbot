//Basically discord-bots-api on npm but for only posting the guild count

var DLA = {};
module.exports = DLA;

DLA.postGuilds = async function(bot, tokens)
{
  var url = "https://botblock.org/api/count";
  var options =
  {
    method: 'POST',
    uri: url,
    body: {
      "server_count": bot.guilds.size,
      "bot_id": bot.user.id,
      "shard_count": bot.shards.size,
      "bots.ondiscord.xyz": tokens.bod,
      "discord.bots.gg": tokens.dbots,
      "discordapps.dev": tokens.dapps,
      "top.gg": tokens.topgg
    },
    json: true
  };
  rp(options).catch(function (err) {
    console.log(err);
  });

  console.log("[DLA] Server Count Posted!");
}

var rp = require('request-promise');
